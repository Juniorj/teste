
const _url = '\action/crud.php';

$(document).on('click', '.cadastrar-atores', function (e) {

    //captura nome
    nome = $('.nome_ator').val();

    $.ajax({
        url: _url,
        type: 'post',
        data: { acao: 'cadastrar-atores', nome: nome },
        success(retorno) {
            l = JSON.parse(retorno);
            if (l.retorno == "success") {
                alert('Operação Realiada com sucesso');
            } else {
                alert('error');
            }
        }
    });
    return false;
});

$(document).on('click', '.editar-atores', function (e) {

    //captura nome
    nome = $('.nome_ator').val();
    id = $('#id').val();
    $.ajax({
        url: _url,
        type: 'post',
        data: { acao: 'editar-atores', nome: nome, id: id },
        success(retorno) {
            l = JSON.parse(retorno);
            if (l.retorno == "success") {
                alert('Operação Realiada com sucesso');
            } else {
                alert('error');
            }
        }
    });
    return false;
});

$(document).on('click', '.deletar-atores', function (e) {

    //captura nome
    id = $(this).attr("data-id");

    
    $.ajax({
        url: _url,
        type: 'post',
        data: { acao: 'deletar-ator', id: id },
        success(retorno) {
            l = JSON.parse(retorno);
            if (l.retorno == "success") {
                alert('Operação Realiada com sucesso');
            } else {
                alert('error');
            }
        }
    });
    return false;
});



$(document).on('click', '.editar-diretor', function (e) {

    //captura nome
    nome = $('.nome_diretor').val();
    id = $('#id').val();
    $.ajax({
        url: _url,
        type: 'post',
        data: { acao: 'editar-diretor', nome: nome, id: id },
        success(retorno) {
            l = JSON.parse(retorno);
            if (l.retorno == "success") {
                alert('Operação Realiada com sucesso');
            } else {
                alert('error');
            }
        }
    });
    return false;
});



$(document).on('click', '.deletar-diretor', function (e) {

    //captura nome
    id = $(this).attr("data-id");
    $.ajax({
        url: _url,
        type: 'post',
        data: { acao: 'deletar-diretor', id: id },
        success(retorno) {
            l = JSON.parse(retorno);
            if (l.retorno == "success") {
                alert('Operação Realiada com sucesso');
            } else {
                alert('error');
            }
        }
    });
    return false;
});



$(document).on('click', '.cadastrar-diretor', function (e) {

    //captura nome
    nome = $('.nome_diretor').val();

    $.ajax({
        url: _url,
        type: 'post',
        data: { acao: 'cadastrar-diretor', nome: nome },
        success(retorno) {
            l = JSON.parse(retorno);
            if (l.retorno == "success") {
                alert('Operação Realiada com sucesso');
            } else {
                alert('error');
            }
        }
    });
    return false;
});


/**
 *  categoria
 */

 
$(document).on('click', '.editar-categoria', function (e) {

    //captura nome
    nome = $('.nome_categoria').val();
    id = $('#id').val();
    $.ajax({
        url: _url,
        type: 'post',
        data: { acao: 'editar-categoria', nome: nome, id: id },
        success(retorno) {
            l = JSON.parse(retorno);
            if (l.retorno == "success") {
                alert('Operação Realiada com sucesso');
            } else {
                alert('error');
            }
        }
    });
    return false;
});



$(document).on('click', '.deletar-categoria', function (e) {

    //captura nome
    id = $(this).attr("data-id");
    $.ajax({
        url: _url,
        type: 'post',
        data: { acao: 'deletar-categoria', id: id },
        success(retorno) {
            l = JSON.parse(retorno);
            if (l.retorno == "success") {
                alert('Operação Realiada com sucesso');
            } else {
                alert('error');
            }
        }
    });
    return false;
});



$(document).on('click', '.cadastrar-categoria', function (e) {

    //captura nome
    nome = $('.nome_categoria').val();

    $.ajax({
        url: _url,
        type: 'post',
        data: { acao: 'cadastrar-categoria', nome: nome },
        success(retorno) {
            l = JSON.parse(retorno);
            if (l.retorno == "success") {
                alert('Operação Realiada com sucesso');
            } else {
                alert('error');
            }
        }
    });
    return false;
});


$(document).on('click', '.deletar-filmes', function (e) {

    //captura nome
    id = $(this).attr("data-id");
    $.ajax({
        url: _url,
        type: 'post',
        data: { acao: 'deletar-filmes', id: id },
        success(retorno) {
            l = JSON.parse(retorno);
            if (l.retorno == "success") {
                alert('Operação Realiada com sucesso');
            } else {
                alert('error');
            }
        }
    });
    return false;
});
