<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


</head>
<?php
require_once(__DIR__ . "/../_app/Config.php");

if (isset($_GET['retorno'])) {
    if ($_GET['retorno'] == 1) :
        echo '<div class="container"></br><div class="alert alert-success" role="alert">Operação Realida com sucesso !</div></div>';
    else :
        echo '<div class="container"></br><div class="alert alert-danger" role="alert">Ops, algo deu errado!</div></div>';
    endif;
}
?>