<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link active" href="index.php">Home</a>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Filmes</a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="filmes.php">Listar</a>
            <a class="dropdown-item" href="filmes_cadastrar.php">Cadastrar</a>
        </div>
    </li>

    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Ator</a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="atores.php">Listar</a>
            <a class="dropdown-item" href="atores_cadastrar.php">Cadastrar</a>
        </div>
    </li>

    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Diretores</a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="diretores.php">Listar</a>
            <a class="dropdown-item" href="diretores_cadastrar.php">Cadastrar</a>
        </div>
    </li>


    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Categorias</a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="categorias.php">Listar</a>
            <a class="dropdown-item" href="categorias_cadastrar.php">Cadastrar</a>
        </div>
    </li>


</ul>