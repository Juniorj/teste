<body>
    <?php require(__DIR__ . "/inc/header.php") ?>
    <div class="container">
        <?php require(__DIR__ . "/inc/menu.php") ?>
        <div class="table-responsive-sm">
            <table class="table table-bordered">
                <?php
                $atores = new Read;
                $atores->ExeRead('select * from categorias');
                $atores->ExeRead('categorias');
                ?>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>Ações</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($atores->getResult() as $atores) :
                    ?>
                        <tr>
                            <td><?= $atores['cod_categoria'] ?></td>
                            <td><?= $atores['nome_categoria'] ?></td>
                        
                        <td>
                                <a href="<?=URL?>/categorias_editar.php?id=<?=$atores['cod_categoria']?>">
                                <button type="button" class="btn btn-info">Editar</button>
                            </a>
                                <button data-id="<?= $atores['cod_categoria'] ?>" type="button" class="btn btn-danger deletar-diretor">Excluir</button>
                        </td>
                        
                        
                        
                        </tr>
                    <?php
                    endforeach;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</body>
<?php require(__DIR__ . "/inc/footer.php") ?>