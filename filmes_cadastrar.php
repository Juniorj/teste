<body>
    <?php require(__DIR__ . "/inc/header.php") ?>
    <div class="container">
        <?php require(__DIR__ . "/inc/menu.php") ?>
        <?php

        $atores    = new Read;
        $diretor   = new Read;
        $categoria = new Read;

        ?>
        <div class="table-responsive-sm">
            <form action="action/cadastrar_filme.php" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="Nome">Titulo:</label>
                    <input type="text" name="nome_filme" class="form-control titulo" placeholder="Enter Nome" id="Nome">
                </div>
                <div class="form-group">
                    <label for="Nome">Descrição:</label>
                    <textarea name="descricao" class="form-control nome_ator">
                    </textarea>
                </div>
                <div class="form-group">
                    <label for="Nome">Categoria:</label>
                    <select name="cod_categoria" class="form-control">
                        <?php
                        $atores->ExeRead('select * from categorias');
                        foreach ($atores->getResult() as $ator) :
                        ?>
                            <option value="<?= $ator['cod_categoria'] ?>"><?= $ator['nome_categoria'] ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="Nome">Ator:</label>
                    <select name="cod_ator" class="form-control">
                        <?php
                        $atores->ExeRead('select * from atores');
                        foreach ($atores->getResult() as $ator) :
                        ?>
                            <option value="<?= $ator['cod_ator'] ?>"><?= $ator['nome_ator'] ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="Nome">Diretor:</label>
                    <select name="cod_diretor" class="form-control">
                    <?php
                        $atores->ExeRead('select * from diretores');
                        foreach ($atores->getResult() as $ator) :
                        ?>
                            <option value="<?= $ator['cod_diretor'] ?>"><?= $ator['nome'] ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="Nome">Imagem:</label>
                    <input type="file" name="imagem">
                </div>



                <button type="submit" id="cadastrar-filme" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>
</body>
<?php require(__DIR__ . "/inc/footer.php") ?>