<body>
    <?php require(__DIR__ . "/inc/header.php") ?>
    <div class="container">
        <?php require(__DIR__ . "/inc/menu.php") ?>
        <?php

        $atores    = new Read;
        $diretor   = new Read;
        $categoria = new Read;

        $filme = new Read;
        $filme->ExeRead('select 
        *
        from filmes 
        inner join categorias on categorias.cod_categoria = filmes.cod_categoria
        inner join diretores on diretores.cod_diretor = filmes.cod_diretor
        inner join atores on atores.cod_ator = filmes.cod_ator');
        $dados = $filme->getResult()[0];
        ?>
        <div class="table-responsive-sm">
            <form action="action/editar_filme.php" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="hidden" name="id" value="<?=$dados['cod_filmes']?>">
                    <label for="Nome">Titulo:</label>
                    <input type="text" name="nome_filme" value="<?=$dados['nome_filme']?>" class="form-control titulo" placeholder="Enter Nome" id="Nome">
                </div>
                <div class="form-group">
                    <label for="Nome">Descrição:</label>
                    <textarea name="descricao"  class="form-control nome_ator">
                    <?=$dados['descricao']?>
                    </textarea>
                </div>
                <div class="form-group">
                    <label for="Nome">Categoria:</label>
                    <select name="cod_categoria" class="form-control">
                        <?php
                        $atores->ExeRead('select * from categorias');
                        foreach ($atores->getResult() as $ator) :
                        ?>
                            <option <?= ($dados['cod_categoria'] == $ator['cod_categoria'])? 'selected': ''?>  value="<?= $ator['cod_categoria'] ?>"><?= $ator['nome_categoria'] ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="Nome">Ator:</label>
                    <select name="cod_ator" class="form-control">
                        <?php
                        $atores->ExeRead('select * from atores');
                        foreach ($atores->getResult() as $ator) :
                        ?>
                            <option <?=($dados['cod_ator'] == $ator['cod_ator'])? 'selected': ''?> value="<?= $ator['cod_ator'] ?>"><?= $ator['nome_ator'] ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="Nome">Diretor:</label>
                    <select name="cod_diretor" class="form-control">
                    <?php
                        $atores->ExeRead('select * from diretores');
                        foreach ($atores->getResult() as $ator) :
                        ?>
                            <option <?= ($dados['cod_diretor'] == $ator['cod_diretor'])? 'selected': ''?> value="<?= $ator['cod_diretor'] ?>"><?= $ator['nome'] ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="Nome">Imagem:</label>
                    <input type="file" name="imagem">
                </div>



                <button type="submit" id="cadastrar-filme" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>
</body>
<?php require(__DIR__ . "/inc/footer.php") ?>