<body>
    <?php require(__DIR__ . "/inc/header.php") ?>
    <div class="container">
        <?php require(__DIR__ . "/inc/menu.php") ?>
        <div class="table-responsive-sm">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>Age</th>
                        <th>City</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Anna</td>
                        <td>Pitt</td>
                        <td>35</td>
                        <td>New York</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>
<?php require(__DIR__ . "/inc/footer.php") ?>