/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50731
Source Host           : localhost:3306
Source Database       : filmes

Target Server Type    : MYSQL
Target Server Version : 50731
File Encoding         : 65001

Date: 2021-02-02 16:47:47
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `atores`
-- ----------------------------
DROP TABLE IF EXISTS `atores`;
CREATE TABLE `atores` (
  `cod_ator` int(11) NOT NULL AUTO_INCREMENT,
  `nome_ator` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_ator`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of atores
-- ----------------------------
INSERT INTO atores VALUES ('5', 'AS AVENTURAS DE LUPIN                    ', '1');
INSERT INTO atores VALUES ('6', 'AS AVENTURAS DE LUPIN                    ', '1');

-- ----------------------------
-- Table structure for `categorias`
-- ----------------------------
DROP TABLE IF EXISTS `categorias`;
CREATE TABLE `categorias` (
  `cod_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `nome_categoria` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cod_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of categorias
-- ----------------------------
INSERT INTO categorias VALUES ('1', 'Terror', null);
INSERT INTO categorias VALUES ('2', 'Ação', '1');

-- ----------------------------
-- Table structure for `diretores`
-- ----------------------------
DROP TABLE IF EXISTS `diretores`;
CREATE TABLE `diretores` (
  `cod_diretor` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_diretor`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of diretores
-- ----------------------------
INSERT INTO diretores VALUES ('5', 'fghfhf', '1');

-- ----------------------------
-- Table structure for `filmes`
-- ----------------------------
DROP TABLE IF EXISTS `filmes`;
CREATE TABLE `filmes` (
  `cod_filmes` int(11) NOT NULL AUTO_INCREMENT,
  `cod_categoria` int(11) NOT NULL,
  `cod_diretor` int(11) NOT NULL,
  `nome_filme` varchar(45) DEFAULT NULL,
  `imagem` varchar(150) DEFAULT NULL,
  `descricao` text,
  `status` int(11) DEFAULT NULL,
  `cod_ator` int(11) NOT NULL,
  PRIMARY KEY (`cod_filmes`),
  KEY `fk_filmes_categoria_idx` (`cod_categoria`),
  KEY `fk_filmes_diretor1_idx` (`cod_diretor`),
  KEY `fk_filmes_atores1_idx` (`cod_ator`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of filmes
-- ----------------------------
INSERT INTO filmes VALUES ('9', '1', '5', 'LUPIN ', '20210202073845.jpg', 'testzao', '1', '5');
