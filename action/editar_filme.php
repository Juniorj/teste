<?php

include('../_app/Config.php');

$id = (int) $_POST['id'];


$data = [
    'cod_categoria' => $_POST['cod_categoria'],
    'cod_diretor' => $_POST['cod_diretor'],
    'nome_filme' => $_POST['nome_filme'],
    'descricao' => $_POST['descricao'],
    'status' => 1,
    'cod_ator' => $_POST['cod_ator']
];


if (isset($_FILES['imagem']['tmp_name'])) {
    $ext = strtolower(substr($_FILES['imagem']['name'], -4));
    $novo_nome = date('Ymdhis') . $ext;
    $dir = '../upload/';

    move_uploaded_file($_FILES['imagem']['tmp_name'], $dir . $novo_nome);

    $imagem = $novo_nome;

    $data + array('imagem' => $imagem);
}

$Upload  = new Update;
$Upload->ExeUpdate('filmes', $data, 'where cod_filmes = id', "id={$id}");

if (!$Upload->getResult()) :
    header('Location: ' . URL . "/filmes.php?retorno=1");

endif;
header('Location: ' . URL . "/filmes.php?retorno=1");

