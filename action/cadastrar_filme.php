<?php

include('../_app/Config.php');


if (isset($_FILES['imagem']['tmp_name'])) {
    $ext = strtolower(substr($_FILES['imagem']['name'], -4));
    $novo_nome = date('Ymdhis') . $ext;
    $dir = '../upload/';
    move_uploaded_file($_FILES['imagem']['tmp_name'], $dir . $novo_nome);
    $imagem = $novo_nome;
} else {
    $imagem = "default.png";
}

$data = [
    'cod_categoria' => $_POST['cod_categoria'],
    'cod_diretor' => $_POST['cod_diretor'],
    'nome_filme' => $_POST['nome_filme'],
    'imagem' => $imagem,
    'descricao' => $_POST['descricao'],
    'status' => 1,
    'cod_ator' => $_POST['cod_ator']
];

$Create  = new Create;
$Create->ExeCreate('filmes', $data);
var_dump($Create->getResult());

if (!$Create->getResult()) :
    header('Location: ' . URL . "/filmes.php?retorno=1");

endif;
header('Location: ' . URL . "/filmes.php?retorno=1");