<?php
//config
define('host', 'localhost');
define('user', 'root');
define('senha', '');
define('banco', 'filmes');
define('diretorio', 'lovalhost');

define('URL', 'http://localhost/projeto_teste/');

function __autoload($class)
{
    $cDir = ['_conn'];
    $iDir = null;
    foreach ($cDir as $dirName) :
        $file = __DIR__ . DIRECTORY_SEPARATOR . $dirName . DIRECTORY_SEPARATOR . $class . '.class.php';
        if (!$iDir && file_exists($file)) :
            include_once($file);
        endif;
    endforeach;


    if (!$iDir) {
        // 'error';
    }
}
