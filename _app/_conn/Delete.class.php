<?php

class Delete extends Conn
{
    private  $Tabela;
    private  $Termos;
    private  $Places;
    private  $Result;

    /** @var PDOStatement */
    private $Delete;

    /** @var PDO */
    private $Conn;


    public function ExeDelete($tabela, $termos, $parseString)
    {
        $this->Tabela = (string) $tabela;
        $this->Termos = (string) $termos;

        parse_str($parseString, $this->Places);
        $this->getSyntax();
        $this->Execute();
    }


    private function getSyntax()
    {
        $this->Delete = "DELETE FROM {$this->Tabela} {$this->Termos}";
    }

    public function setPlaces($parseString)
    {
        parse_str($parseString, $this->Places);
        $this->getSyntax();
        $this->Execute();
    }
    /**
     *  RECUPERA O PDO PARA FAZER CONECXAO , DA MINHA CLASS PAI
     */
    public function Connect()
    {
        $this->Conn = parent::getConn();
        $this->Delete = $this->Conn->prepare($this->Delete);
        
    }

    

    //** get para retorno resultado */
    public function getResult()
    {
        return $this->Result;
    }


    //** executar a query */
    private function Execute()
    {
        $this->Connect();
        try {
            $this->Delete->execute($this->Places);
            $this->Result = true;
        } catch (PDOException $e) {
            $this->Result = $e->getMessage();
            return $e->getCode() . '-' . $e->getMessage();
        }
    }
}
