<?php

class Read extends Conn
{
    private  $Select;
    private  $Places;
    private  $Result;

    /** @var PDOStatement */
    private $Read;

    /** @var PDO */
    private $Conn;

    /**
     * 
     */
    public function ExeRead($query, $ParseString = null)
    {
       $this->Select = (string) $query;
        if (!empty($ParseString)) :
            parse_str($ParseString, $this->Places);
        endif;
        $this->Execute();
    }

    public function setPlaces($ParseString)
    {
        parse_str($ParseString, $this->Places);
        $this->Execute();
    }


    /**
     *  RECUPERA O PDO PARA FAZER CONECXAO , DA MINHA CLASS PAI
     */
    public function Connect()
    {
        $this->Conn = parent::getConn();
        $this->Read = $this->Conn->prepare($this->Select);
        $this->Read->setFetchMode(PDO::FETCH_ASSOC);
    }

    //** get para retorno resultado */
    public function getResult()
    {
        return $this->Result;
    }


    //** preparar a query */
    private function getSyntax()
    {
        #$sth->bindValue(':calories', $calories, PDO::PARAM_INT);
        #$sth->bindValue(':colour', $colour, PDO::PARAM_STR);
        if ($this->Places) :
            foreach ($this->Places as $vinculo => $valor) :
                $type = (is_int($valor)) ?  PDO::PARAM_INT : PDO::PARAM_STR;
                $this->Read->bindValue(":{$vinculo}", $valor, $type);
            endforeach;
        endif;
    }

    //** executar a query */
    private function Execute()
    {
        $this->Connect();
        try {
            $this->getSyntax();
            $this->Read->execute();
            $this->Result = $this->Read->fetchAll();
        } catch (PDOException $e) {
            $this->Result = null;
            return $e->getCode() . '-' . $e->getMessage();
        }
    }
}
