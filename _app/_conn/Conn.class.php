<?php


abstract class Conn
{
    private static $Host = 'localhost';
    private static $User = 'root';
    private static $Pass = '';
    private static $Dba  = 'filmes';

    private static $Connect = null;


    private static function Conectar()
    {
        try {

            if (self::$Connect == null) :

                $dns = 'mysql:host=' . self::$Host . ';dbname=' . self::$Dba;
                $opt = [PDO::MYSQL_ATTR_INIT_COMMAND  => 'SET NAMES UTF8'];

                self::$Connect = new PDO($dns, self::$User, self::$Pass, $opt);
            endif;

            self::$Connect->SetAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return self::$Connect;
        } catch (PDOException $e) {
            return $e->getCode() . '-' . $e->getMessage();
        }
    }

    protected static function getConn()
    {
        return self::Conectar();
    }
}
