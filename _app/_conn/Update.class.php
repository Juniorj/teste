<?php

class Update extends Conn
{


    private $Tabela;
    private $Dados;
    private $Termos;
    private $Places;
    private $Result;

    /** @var PDOStatement */
    private $Update;

    /** @var PDO */
    private $Conn;

    /**
     * 
     * 
     */
    public function ExeUpdate($Tabela, array $Dados, $termos, $ParseString)
    {
        $this->Tabela = (string) $Tabela;
        $this->Dados = $Dados;
        $this->Termos  = (string) $termos;

        parse_str($ParseString, $this->Places);
        $this->getSyntax();
        $this->Execute();
    }

    public function setPlaces($ParseString)
    {
        parse_str($ParseString, $this->Places);
        $this->getSyntax();
        $this->Execute();
    }


    /**
     *  RECUPERA O PDO PARA FAZER CONECXAO , DA MINHA CLASS PAI
     */
    public function Connect()
    {
        $this->Conn = parent::getConn();
        $this->Update = $this->Conn->prepare($this->Update);
    }

    //** get para retorno resultado */
    public function getResult()
    {
        return $this->Result;
    }


    //** preparar a query */
    private function getSyntax()
    {
        if ($this->Dados) :
            foreach ($this->Dados as $key => $value) :
                $Places[] = $key . ' = :' . $key;
            endforeach;
        endif;

        $Places = implode(', ', $Places);
        $this->Update = "UPDATE {$this->Tabela} SET {$Places} {$this->Termos}";
    }

    //** executar a query */
    private function Execute()
    {
        $this->Connect();
        try {
            
            $this->Update->execute(array_merge($this->Dados, $this->Places));
            $this->Result = true;
        } catch (PDOException $e) {
            $this->Result = $e->getMessage();
            return $e->getCode() . '-' . $e->getMessage();
        }
    }
}
