<?php

class Create extends Conn
{
    private  $Tabela;
    private  $Dados;
    private  $Result;

    /** @var PDOStatement */
    private $Create;

    /** @var PDO */
    private $Conn;

    /**
     * 
     */
    public function ExeCreate($Tabela, array $Dados)
    {
        $this->Tabela = (string) $Tabela;
        $this->Dados =  $Dados;

        //preparar os dados
        $this->getSyntax();
        //executar
        $this->Execute();
    }

    /**
     *  RECUPERA O PDO PARA FAZER CONECXAO , DA MINHA CLASS PAI
     */
    public function Connect()
    {
        $this->Conn = parent::getConn();
        $this->Create = $this->Conn->prepare($this->Create);
    }

    //** get para retorno resultado */
    public function getResult()
    {
        return $this->Result;
    }


    //** preparar a query */
    private function getSyntax()
    {
        $Fileds = implode(',', array_keys($this->Dados));
        $Places = ':'. implode(', :', array_keys($this->Dados));
        $this->Create = "INSERT INTO {$this->Tabela} ({$Fileds}) VALUES ({$Places})";
    }

    //** executar a query */
    private function Execute()
    {
        $this->Connect();
        try {
            $this->Create->execute($this->Dados);
            /** herança */
            $this->Result  = $this->Conn->lastInsertId();
            
        } catch (PDOException $e) {
            echo $e->getMessage();
            return $e->getCode() . '-' . $e->getMessage();
        }
    }
}
