<body>
    <?php require(__DIR__ . "/inc/header.php") ?>
    <div class="container">
        <?php require(__DIR__ . "/inc/menu.php") ?>
        <div class="table-responsive-sm">
            <table class="table table-bordered">
                <?php

               $atores = new Read;
                $atores->ExeRead('select 
                filmes.nome_filme as filme,
                filmes.cod_filmes,
                categorias.nome_categoria as categoria,
                diretores.nome as diretor,
                atores.nome_ator as ator
                from filmes 
                inner join categorias on categorias.cod_categoria = filmes.cod_categoria
                inner join diretores on diretores.cod_diretor = filmes.cod_diretor
                inner join atores on atores.cod_ator = filmes.cod_ator');
                ?>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>Ator</th>
                        <th>Diretor</th>
                        <th>Categoria</th>
                        <th>Ações</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($atores->getResult() as $atores) :
                    ?>
                        <tr>
                            <td><?= $atores['cod_filmes'] ?></td>
                            <td><?= $atores['filme'] ?></td>
                            <td><?= $atores['ator'] ?></td>
                            <td><?= $atores['diretor'] ?></td>
                            <td><?= $atores['categoria'] ?></td>
                            <td>
                                <a href="<?=URL?>/filmes_editar.php?id=<?=$atores['cod_filmes']?>">
                                <button type="button" class="btn btn-info">Editar</button>
                            </a>
                                <button data-id="<?= $atores['cod_filmes'] ?>" type="button" class="btn btn-danger deletar-filmes">Excluir</button>
                            </td>
                        </tr>
                    <?php
                    endforeach;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</body>
<?php require(__DIR__ . "/inc/footer.php") ?>