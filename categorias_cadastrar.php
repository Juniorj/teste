<body>
    <?php require(__DIR__ . "/inc/header.php") ?>
    <div class="container">
        <?php require(__DIR__ . "/inc/menu.php") ?>
        <div class="table-responsive-sm">
                <div class="form-group">
                    <label for="Nome">Nome:</label>
                    <input type="text" class="form-control nome_categoria" placeholder="Enter Nome" id="nome_categoria">
                </div>
                <button  id="cadastrar-categoria" class="btn btn-primary cadastrar-categoria">Submit</button>
        </div>
    </div>
</body>
<?php require(__DIR__ . "/inc/footer.php") ?>